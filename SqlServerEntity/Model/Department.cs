﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SqlServerEntity.Model
{
    class Department
    {
	    [Key]
	    public int Id { get; set; }
	    public string Name { get; set; }
	}
}
